#include <iostream>
#include <fstream>
#include <htslib/faidx.h>

#include "argumentparser.h"
#include "bedio.h"
#include "version.h"

int main_utrseq (const int argc, const char *argv[])
{
    std::string fileBed;
    std::string fileFasta;
    int utrType;

    auto p = ArgumentParser("utrseq", std::string(VERSION), "extract UTR sequence");
    p.addArgumentRequired("annotation").setKeyShort("-b").setKeyLong("--bed").setHelp("BED file containing transcript annotation");
    p.addArgumentPositional("sequence").setKeyShort("-f").setKeyLong("--fasta").setHelp("FASTA file containing transcript sequence");
    p.addArgumentOptional("utr").setKeyShort("-u").setKeyLong("--utr").setHelp("[5] or 3 prime UTR choice, 0 for ORF").setDefaultValue<int>(5);
    p.addArgumentFlag("help").setKeyShort("-h").setKeyLong("--help").setHelp("prints help message");
    p.addArgumentFlag("version").setKeyShort("-v").setKeyLong("--version").setHelp("prints major.minor.build version");

    try {
        p.parse(argc, argv);
        fileBed = p.get<std::string>("annotation");
        fileFasta = p.get<std::string>("sequence");
        utrType = p.get<int>("utr");
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }


    // open BED file
    auto hBed = BedIO(fileBed);
    if (!hBed.isOpen()) {
        std::cerr << "ribotools::" + hBed.error() << std::endl;
        return EXIT_FAILURE;
    }


    // open FASTA file
    faidx_t *fhFai = fai_load(fileFasta.c_str());
    if (!fhFai) {
        std::cerr << "ribotools::codonrate::error, failed to load fasta reference " << fileFasta << std::endl;
        return 1;
    }

    int records = 0;
    while (hBed.next()) {

        std::size_t fputrSpan = static_cast<std::size_t>(hBed.bed().orfStart());
        std::size_t tputrSpan = static_cast<std::size_t>(hBed.bed().span() - hBed.bed().orfEnd());
        std::size_t orfSpan = static_cast<std::size_t>(hBed.bed().orfSpan());

        if ((fputrSpan <= 3) && (utrType == 5))
            continue;

        if ((tputrSpan <= 3) && (utrType == 3))
            continue;

        if ((orfSpan <= 3) && (utrType == 0))
            continue;

        int sequence_size = 0;
        char *sequence = faidx_fetch_seq(fhFai, hBed.bed().name(0).c_str(), 0, hBed.bed().span(), &sequence_size);
        if (!sequence)
            continue;
        std::string seq_transcript(sequence);

        if (utrType == 5)
            std::cout << hBed.bed().name(0) << "\t" << seq_transcript.substr(0, fputrSpan) << std::endl;
        else if (utrType == 3)
            std::cout << hBed.bed().name(0) << "\t" << seq_transcript.substr(static_cast<std::size_t>(hBed.bed().orfEnd()), tputrSpan) << std::endl;
        else if (utrType == 0)
            std::cout << hBed.bed().name(0) << "\t" << seq_transcript.substr(static_cast<std::size_t>(hBed.bed().orfStart()), orfSpan) << std::endl;
        records++;
    }

    std::cerr << "records: " << records << std::endl;

    return 0;
}
