classdef TabixInterface < handle
    %TABIXINTERFACE
    % MATLAB class wrapper for the underlying C++ class
    
    properties (SetAccess = private, Hidden = true)
        objectHandle; % Handle to the underlying C++ class instance
    end
    
    methods
        %% Constructor - Create a new C++ class instance 
        function this = TabixInterface(varargin)
            this.objectHandle = TabixInterfaceMex('new', varargin{:});
        end
        
        %% Destructor - Destroy the C++ class instance
        function delete(this)
            TabixInterfaceMex('delete', this.objectHandle);
        end

        %% Open - open HTS file
        function varargout = open(this, varargin)
            [varargout{1:nargout}] = TabixInterfaceMex('open', this.objectHandle, varargin{:});
        end

        %% isOpen - test if file is open
        function varargout = isOpen(this, varargin)
            [varargout{1:nargout}] = TabixInterfaceMex('isOpen', this.objectHandle, varargin{:});
        end
        
        %% query - returns coverage
        function varargout = query(this, varargin)
            [varargout{1:nargout}] = TabixInterfaceMex('query', this.objectHandle, varargin{:});
        end
        
    end
end

