%% codonNFCtoMFDR
clc
clear variables
close all

%% read reference codons
%{
fh = fopen('/Users/tushevg/Desktop/data/codonTable.txt', 'r');
txt = textscan(fh, '%s %s %s %s','delimiter','\t');
fclose(fh);
ref.code = txt{1};
ref.letter = txt{2};
ref.abbr = txt{3};
ref.name = txt{4};
%}


%% parse files
pathToNFC = '/Users/tushevg/Desktop/ElongationRate/mtdr/nfc';
fileList = dir([pathToNFC, filesep, '*.txt']);
fileCount = length(fileList);

%% prepare output
pathToTDT = '/Users/tushevg/Desktop/ElongationRate/mtdr/adr';


%% parse each file
%f = 1;
for f = 1 : fileCount

    % file names
    fileNFC = [pathToNFC, filesep, fileList(f).name];
    [~, fileName] = fileparts(fileNFC);
    fileTag = regexp(fileName,'\_','split');
    fileName = regexprep(fileName, [fileTag{1},'_'], '');
    fileADR = [pathToTDT, filesep, 'tableADR_', fileName, '.txt'];
    disp(fileName);
    
    % read NFC
    nfc = readNFCTable(fileNFC, 50);
    param = zeros(64, 3);
    count = zeros(64, 1);
    %a = 1;
    for a = 1 : 64
        Y = nfc.value(nfc.index == a);
        
        lambda = std(Y) .* 0.8;
        mu = mean(Y) - skewness(Y);
        sigma = sqrt(var(Y) - (lambda^2));
        b0 = [mu, sigma, lambda];
        
        mdl = fminsearch(@(params)logLHexGauss(params, Y), b0);
        param(a,:) = mdl;
        count(a) = length(Y);
    end
    
    % write ADR table
    writeADRTable(fileADR, nfc.aa, count, param);

end


function nfc = readNFCTable(fileNFC, upperBound)

    fh = fopen(fileNFC, 'r');
    txt = textscan(fh, '%s %n','delimiter','\t');
    fclose(fh);
    nfc.code = txt{1};
    nfc.value = txt{2};
    idxFilter = nfc.value > upperBound;
    nfc.code(idxFilter) = [];
    nfc.value(idxFilter) = [];
    [nfc.aa, ~, nfc.index] = unique(nfc.code);
    
end

function writeADRTable(fileADR, aaList, count, param)

    fw = fopen(fileADR, 'w');
    fprintf(fw,'#code\tcount\tdecoding.rate.avg\tdecoding.rate.std\tpausing.rate\n');
    for k = 1 : length(aaList)
        fprintf(fw,'%s\t',aaList{k});
        fprintf(fw,'%d\t',count(k));
        fprintf(fw,'%.8f\t',param(k,1));
        fprintf(fw,'%.8f\t',param(k,2));
        fprintf(fw,'%.8f\n',param(k,3));
    end
    fclose(fw);
    
end


function logL = logLHexGauss(params, x)

    y = exGaussPDF(params, x) + eps;
    logL = -sum(log(y));

end

function f = exGaussPDF(params, x)
    
    mu = params(1);
    sigma = params(2);
    lambda = params(3);
    
    argGauss = exp((lambda ./ 2) .* (2 .* mu + lambda .* sigma .^ 2 - 2 .* x));
    argExp = erfc((mu + lambda .* sigma .^ 2 - x) ./ (sqrt(2) .* sigma));
    f = (lambda ./ 2) .* argGauss .* argExp;
    
end
