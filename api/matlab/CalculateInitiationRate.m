%% initiationRateTest
clc
clear variables
close all

%% read table
fmt = repmat({'%n'}, 18, 1);
fmt(1) = {'%s'};
fmt = sprintf('%s ', fmt{:});
fmt(end) = [];

fh = fopen('~/Desktop/tableIRateCounts_Somata_13Mar2020.txt', 'r');
txt = textscan(fh, fmt, 'delimiter', '\t');
fclose(fh);
name = txt{1};
lengthGene = txt{2};
lengthCDS = txt{3};
totalRNA = [txt{4:5:end}];
cdsRNA = [txt{5:5:end}];
totalRFP = [txt{6:5:end}];
cdsRFP = [txt{7:5:end}];
iniRFP = [txt{8:5:end}];

%
%% filter empty
%
idx = all(iniRFP > 0, 2) & all(cdsRFP > 0,2) & all(cdsRNA > 0, 2);
name(~idx) = [];
lengthGene(~idx) = [];
lengthCDS(~idx) = [];
totalRFP(~idx,:) = [];
totalRNA(~idx,:) = [];
cdsRFP(~idx,:) = [];
cdsRNA(~idx,:) = [];
iniRFP(~idx,:) = [];

%% normalize to TPM
cdsRFP_tpm = normalizeTPM(cdsRFP, lengthCDS - 3);
cdsRNA_tpm = normalizeTPM(cdsRNA, lengthGene);
iniRFP_tpm = normalizeTPM(iniRFP, 30);

%% normalize to RPKM
cdsRFP_rpkm = normalizeRPKM(cdsRFP, lengthCDS - 3);
cdsRNA_rpkm = normalizeRPKM(cdsRNA, lengthGene);
iniRFP_rpkm = normalizeRPKM(iniRFP, 30);

%% PCA
%{
[cf, ~, laTEdeseqt] = pca([iniRFP_tpm, cdsRFP_tpm, cdsRNA_tpm]);
pv = 100.*laTEdeseqt/sum(laTEdeseqt);

figure('color','w');
hold on;
h(1) = plot(cf(1:3,1), cf(1:3,2),'k*', 'markersize', 8);
h(2) = plot(cf(4:6,1), cf(4:6,2),'k.', 'markersize', 8);

h(3) = plot(cf(7:9,1), cf(7:9,2),'g*', 'markersize', 8);
h(4) = plot(cf(10:12,1), cf(10:12,2),'g.', 'markersize', 8);

h(5) = plot(cf(13:15,1), cf(13:15,2),'c*', 'markersize', 8);
h(6) = plot(cf(16:18,1), cf(16:18,2),'c.', 'markersize', 8);

hold off;
xlabel(sprintf('PC1 (%.2f%%)',pv(1)),'fontsize',12);
ylabel(sprintf('PC2 (%.2f%%)',pv(2)),'fontsize',12);
hl = legend(h,'RFP initiation neuropil',...
              'RFP initiation somata',...
              'RFP neuropil',...
              'RFP somata',...
              'RNA neuropil',...
              'RNA somata');
set(hl,'edgecolor','w','location','northeast','fontsize',12);
print(gcf, '-dpng','-r300','figureX_PCA_Samples.png');
%}


%% calculate TE
TEtpm = cdsRFP_tpm ./ cdsRNA_tpm;
TErpkm = cdsRFP_rpkm ./ cdsRNA_rpkm;

X = [cdsRFP, cdsRNA] + 1;
Xn = DESeqNormalization(X);
TEdeseq = Xn(:,1:3)./Xn(:,4:end);

Y = [iniRFP, cdsRFP] + 1;
Yn = DESeqNormalization(Y);


Z = zscore(log2(TEtpm));
ZZ = 2.^Z;
TEtpm = ZZ;

idxAdjust = TEtpm > 1;
ksi = zeros(1, 3);
for r = 1 : 3
    ksi(r) = 1 / prctile(TEtpm(idxAdjust(:,r),r), 99);
end



V = log2(TEtpm(:));
%ksi = mean(ksi); %0.0084140/2; % 0.0055069; % 0.0015;
%Lksi = log2(ksi);

[f,x] = histcounts(V, 100);
xx = x(1:end-1);

%{
figure('color','w');
h(1) = bar(xx,f,'edgecolor','none','facecolor',[.75,.75,.75]);
hold on;
plot([0,0],[0,max(f)],'k','linewidth',1.2);
h(2) = plot(xx + log2(0.015), f,'color',[148,0,211]./255,'linewidth',1.2);
h(3) = plot(xx + log2(ksi), f, 'color',[30,144,255]./255,'linewidth',1.2);
hold off;
set(gca,'box','off','fontsize',12);
hl = legend(h,'TE','0.015 * TE', sprintf('%.4f * TE',ksi));
set(hl,'edgecolor','w','location','northwest');
xlabel('translation efficiency (TE) [log_2(relative fraction)]','fontsize',12);
ylabel('frequency [genes]','fontsize',12);
%print(gcf,'-dpng','-r300','figureIRate_TEScaling.png');
%}

%{
ksi = zeros(1, size(TE, 2));
for k = 1 : size(TE, 2)
    V = TE(:,k);
    ksi(k) = 1 / prctile(V(V > 1), 95);
end
TE_adj = bsxfun(@times, TE, ksi);
%}




%% calculate initiation rate
%
%f = totalRFP(1,:)./totalRNA(1,:);
Nc_1 = lengthCDS./3 - 1;
T_exp = Nc_1 .* (1/3.5); % 3.5 AA / sec as a rate
p_ji = iniRFP ./ cdsRFP;
p_ji_n = (Yn(:,1:3) ./ Yn(:,4:end))./15;
ro_exp = (ksi .* TEtpm);
ro_ji = p_ji_n .* ro_exp .* (Nc_1/12);

alpha = (ro_exp .* Nc_1) ./ (T_exp .* (1 - ro_ji));


idxUse = all((0 < alpha) & (alpha <= 5), 2);

V = alpha(idxUse,:);
[f, x] = histcounts(V(:), 100);
figure('color','w');
h = bar(x(1:end-1),100.*f./sum(f), 'edgecolor','none','facecolor',[.75,.75,.75]);
set(gca,'box','off','fontsize',12);
xlabel('initiation rate [events / sec]','fontsize',12);
ylabel('fraction [%]','fontsize',12);
%print(gcf, '-dpng','-r300','figureIRate_Histogram.png');
%}

%% write table
%
txt = [name, num2cell(alpha)]';

txt = sprintf('%s\t%.4f\t%.4f\t%.4f\n',txt{:});
fw = fopen('~/Desktop/tableIRate_Somata_TPM_18Mar2020.txt','w');
fprintf(fw,'%s',txt);
fclose(fw);
%}


%{
figure('color','w');
boxplot(alpha(idx,:),'color',[0,0,0;120,120,120]./255,'colorgroup',[1,1,1,2,2,2],'symbol','');
set(gca,'box','off','ylim',[0,0.03],...
    'xtick',[2,5],...
    'xticklabels',{'neuropil', 'somata'},'fontsize',12);
ylabel('initiation rate [sec^-^1]');
print(gcf, '-dpng','-r300','figureX_IRateBoxplot.png');
%}

%p = mattest(alpha(:,1:3), alpha(:,4:6));
%fc = log2(mean(alpha(:,1:3)./alpha(:,4:6), 2));

%figure('color','w')
%plot(log10(ro_ji(:,1)), log10(ro_exp(:,1)), '.');


%% Adjust TE
%{
X = log10(Xn(:,7));
Yraw = log10(Xn(:,1));
Y = 0.15.*log10(Xn(:,7));

b = robustfit(X, Y);
b = flipud(b);

Xfit = linspace(-2, 5, 100);
Yfit = polyval(b, Xfit);

figure('color','w');
plot(X, Yraw, '.','color',[30,144,255]./255);
hold on;
plot((1:5),(1:5),'k','linewidth',1.2);
hold off;
set(gca,'box','off',...
'xlim',[1,5],...
'ylim',[1,5],...
'fontsize',12);
xlabel('RNA [log_1_0(TPM)]','fontsize',12);
ylabel('RFP [log_1_0(TPM)]','fontsize',12);
title('Relative Translation Efficiency TE = RFP / RNA','fontsize',12,'fontweight','normal');
print(gcf,'-dpng','-r300','figureX_TE_raw.png');
figure('color','w');
plot(X, Y, '.','color',[30,144,255]./255);
hold on;
plot((1:5),(1:5),'k','linewidth',1.2);
plot(Xfit, Yfit, 'r');
hold off;
set(gca,'box','off',...
'xlim',[1,5],...
'ylim',[-5,5],...
'fontsize',12);
xlabel('RNA [log_1_0(TPM)]','fontsize',12);
ylabel('RFP [log_1_0(TPM)]','fontsize',12);
title('Adjusted Relative Translation Efficiency TE = ksi * RFP / RNA','fontsize',12,'fontweight','normal');
print(gcf,'-dpng','-r300','figureX_TE_adjusted.png');
%}


%{
TE = TE_adj;
figure('color','w');
boxplot(log10(TE),'color',[0,0,0;120,120,120]./255,'colorgroup',[1,1,1,2,2,2],'symbol','');
set(gca,'box','off','ylim',[-2,0.5],...
    'xtick',[2,5],...
    'xticklabels',{'neuropil', 'somata'},'fontsize',12);
ylabel('TE [log_1_0(RFP / RNA )]');
print(gcf, '-dpng', '-r300','figureX_TEboxplot.png');

figure('color','w');
[cf, ~, laTEdeseqt] = pca(zscore(TE));
hold on;
h(1) = plot(cf(1:3,1), cf(1:3,2), 'k.','markersize',8);
h(2) = plot(cf(4:6,1), cf(4:6,2), 'g.','markersize',8);
hold off;
pv = 100.*laTEdeseqt/sum(laTEdeseqt);
xlabel(sprintf('PC1 (%.2f%%)',pv(1)),'fontsize',12);
ylabel(sprintf('PC2 (%.2f%%)',pv(2)),'fontsize',12);
hl = legend(h,'neuropil',...
              'somata');
set(hl,'edgecolor','w','location','northeast','fontsize',12);
print(gcf, '-dpng', '-r300', 'figureX_TEPCA.png');
%}


%% helpers
function cnts_tpm = normalizeTPM(cnts, span)

    cnts_rpk = cnts ./ (span ./ 1000);
    cnts_tpm = bsxfun(@rdivide, cnts_rpk, sum(cnts_rpk)./1e6);
    
end


function cnts_rpkm = normalizeRPKM(cnts, span)

    cnts_rpm = bsxfun(@rdivide, cnts, sum(cnts)./ 1e6);
    cnts_rpkm = cnts_rpm ./ (span ./ 1000);

end


%{
repA = readIRate('irateTable_test01.txt');
repB = readIRate('irateTable_test02.txt');
repC = readIRate('irateTable_test04.txt');

list = intersect(repA.name, intersect(repB.name,repC.name));
[~,idxA] = intersect(repA.name, list);
[~,idxB] = intersect(repB.name, list);
[~,idxC] = intersect(repC.name, list);

N = repA.name(idxA);
V = [repA.readsRFPcds(idxA),...
     repB.readsRFPcds(idxB),...
     repC.readsRFPcds(idxC),...
     repA.readsRNAcds(idxA),...
     repB.readsRNAcds(idxB),...
     repC.readsRNAcds(idxC)];
L = repA.cdsSize(idxA);

%% normalize to TPM
V_RPK = V ./ (L ./ 1000);
V_TPM = bsxfun(@rdivide, V_RPK, sum(V_RPK)./1e6);
TE = V_TPM(:,1:3) ./ V_TPM(:,4:6);

%% probability of initiation
P = [repA.readsRFPini(idxA)./repA.readsRFPcds(idxA),...
     repB.readsRFPini(idxB)./repB.readsRFPcds(idxB),...
     repC.readsRFPini(idxC)./repC.readsRFPcds(idxC)];
    
%% deseq normalisation
[cnt, fctr] = DESeqNormalization(V);
TEdeseq = cnt(:,1:3) ./ cnt(:,4:6);
Nc_1 = L./3 - 1;
T_exp = Nc_1 .* (1/3.5); % 3.5 AA / sec as a rate

ro_exp = TEdeseq .* 1./max(prctile(TEdeseq,95));
ro_ji = P .* ro_exp .* Nc_1;

figure('color','w');
plot(TE(:,3), TEdeseq(:,3),'.');



function data = readIRate(filename)
    fh = fopen(filename,'r');
    txt = textscan(fh, '%s %n %n %n %n %n %n','delimiter', '\t');
    fclose(fh);
    data.name = txt{1};
    data.cdsSize = txt{2};
    data.readsRFP = txt{3};
    data.readsRFPcds = txt{4};
    data.readsRFPini = txt{5};
    data.readsRNA = txt{6};
    data.readsRNAcds = txt{7};
end
%}
%{
%% normalize to TPM
V = [readsRFPcds, readsRNAcds];
V_RPK = V ./ (cdsSize./1000);
V_TPM = bsxfun(@rdivide, V_RPK, sum(V_RPK)./1e6);
TE = V_TPM(:,1) ./ V_TPM(:,2);

%% constants
ksi = 0.015;
ro_exp = ksi .* TE;
Nc_1 = cdsSize./3 - 1;
p_ini = readsRFPini ./ readsRFPcds;
ro_ji = p_ini .* ro_exp .* Nc_1;
T_exp = Nc_1 .* (1/3.5); % 3.5 AA / sec as a rate

%% initiation rate
alpha = (ro_exp .* Nc_1)./(T_exp .* (1 - ro_ji));
idx = (TE <= 1/ksi) & (ro_ji <= 1);

figure('color','w');
plot(log2(ro_exp), log2(ro_ji), '.');
%}
