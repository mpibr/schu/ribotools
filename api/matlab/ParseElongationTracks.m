%% TestTabixInterface
clc
close all
%
clear variables

%mex TabixInterfaceMex.cpp -I/usr/local/Cellar/htslib/1.10.2/include -I/usr/local/Cellar/xz/5.2.4/include -L/usr/local/Cellar/htslib/1.10.2/lib -lhts;

%% constants
xBins = 400;
xOffset = 20;
fileOrfInfo = '/Users/tushevg/Desktop/ElongationRate/orfinfo/tableOrfInfo_HTClean_SynapticSynDB_28Feb2020.txt';
fileOutPath = '/Users/tushevg/Desktop/ElongationRate/results';
[~, fileOutName] = fileparts(fileOrfInfo);
fileOutName = regexprep(fileOutName, 'tableOrfInfo', 'tableResults');
fileOut = [fileOutPath, filesep, fileOutName, '.txt'];

filesCoverage = {'/Users/tushevg/Desktop/data/gbed/00s_short_01.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/00s_short_02.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/00s_short_03.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/15s_01.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/15s_02.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/15s_03.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/30s_01.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/30s_02.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/30s_03.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/45s_01.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/45s_02.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/45s_03.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/00s_long_01.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/00s_long_02.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/00s_long_03.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/90s_01.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/90s_02.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/90s_03.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/120s_01.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/120s_02.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/120s_03.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/150s_01.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/150s_02.gbed.gz';...
                '/Users/tushevg/Desktop/data/gbed/150s_03.gbed.gz'};
countFilesCoverage = length(filesCoverage);

%% allocate result containers
tracks = zeros(countFilesCoverage, xBins);
label = repmat({'unknown'}, countFilesCoverage, 1);
countUsed = 0;


%% read ORF info
[query, countRaw] = readQueryTable(fileOrfInfo, xBins, xOffset);
countQuery = length(query.name);

%% constructor
for k = 1 : countFilesCoverage
    [~, name] = fileparts(filesCoverage{k});
    name = regexprep(name,'\.gbed','');
    label(k) = {name};
    obj(k) = TabixInterface();
    obj(k).open(filesCoverage{k});
end


%% parse tracks
tic
for q = 1 : countQuery
    
    %% extract data
    data = zeros(countFilesCoverage, query.orfSpan(q) / 3);
    for k = 1 : countFilesCoverage
        track = obj(k).query(query.tid{q},query.orfStart(q),query.orfEnd(q)) + 1;
        data(k,:) = mean(reshape(track, 3, query.orfSpan(q) / 3), 1);
    end
    
    %% skip empty
    if any(all(data == 1, 2))
        continue;
    end
    
    
    %% normalise to the end
    baseline = mean(data(:, xBins + xOffset : end - xOffset), 2);
    if any(baseline < 2)
        continue;
    end
    
    ndata = data ./ baseline;
    
    %% smooth
    sdata = movmean(ndata, 30, 2);
    
    %% accumulate data
    countUsed = countUsed + 1;
    prevWeight = (countUsed - 1) / countUsed;
    nextWeight = 1 / countUsed;
    tracks = prevWeight .* tracks + nextWeight .* sdata(:, xOffset : xBins + xOffset - 1);
    
end
toc

fprintf('records used/raw = %d / %d (%.2f%%)\n', countUsed, countRaw, 100*countUsed/countRaw); 

%% export result
fw = fopen(fileOut, 'w');
for k = 1 : length(label)
    fprintf(fw,'%s\t%d\t%d\t%s\n',label{k}, countUsed, countRaw, sprintf('%.8f,',tracks(k,:)));
end
fclose(fw);




%% destructor
for k = 1 : countFilesCoverage
    obj(k).delete;
end
clear obj;





%% FUNCTIONS
function [meta, count] = readQueryTable(fileName, xBins, xOffset)

    fh = fopen(fileName, 'r');
    txt = textscan(fh, '%s %*n %n %n %n %n', 'delimiter', '\t');
    fclose(fh);
    meta.name = txt{1};
    meta.trsSpan = txt{2};
    meta.orfStart = txt{3};
    meta.orfEnd = txt{4};
    meta.orfSpan = txt{5};
    
    txt = textscan(sprintf('%s\n',meta.name{:}), '%s %s','delimiter', ';');
    meta.tid = txt{1};
    meta.gid = txt{2};
    
    count = size(meta.name, 1);
    
    % filter based on bins and offset
    sizeCodons = meta.orfSpan / 3;
    idxFilter = sizeCodons <= (xBins + 2*xOffset);
    meta.name(idxFilter) = [];
    meta.trsSpan(idxFilter) = [];
    meta.orfStart(idxFilter) = [];
    meta.orfEnd(idxFilter) = [];
    meta.orfSpan(idxFilter) = [];
    meta.tid(idxFilter) = [];
    meta.gid(idxFilter) = [];
    
end