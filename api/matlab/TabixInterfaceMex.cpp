#include <string>
#include <vector>
#include <htslib/hts.h>
#include <htslib/tbx.h>
#include "mex.h"
#include "class_handle.hpp"

class TabixInterface
{
public:
    TabixInterface() : 
        m_isOpen(false),
        m_fh(nullptr),
        m_tbx(nullptr),
        m_iter(nullptr),
        m_buffer(nullptr)
    { 
        //mexPrintf("TabixInterface::constructor\n"); 
    }
    
    ~TabixInterface()
    { 
        if (m_buffer)
            free(m_buffer);
        
        if (m_iter)
            tbx_itr_destroy(m_iter);
        
        if (m_tbx)
            tbx_destroy(m_tbx);
        
        if (m_fh)
            hts_close(m_fh);
        
        //mexPrintf("TabixInterface::destructor\n");
    }
    
    bool open(const char *fileName)
    { 
        // open HTS file
        m_fh = hts_open(fileName, "r");
        if (!m_fh) {
            mexErrMsgTxt("TabixInterface::error, failed to open HTS file\n");
            m_isOpen = false;
            return isOpen();
        }
        
        // load Tabix index
        m_tbx = tbx_index_load(fileName);
        if (!m_tbx) {
            mexErrMsgTxt("TabixInterface::error, failed to laod Tabix index\n");
            m_isOpen = false;
            return isOpen();
        }
        
        // successful
        m_isOpen = true;
        
        return isOpen();
    };
    
    bool isOpen() const 
    { 
        //mexPrintf("TabixInterface::isOpen %d\n", m_isOpen);
        return m_isOpen; 
    }
    
    void query(int *depth, const char *chrom, int chromStart, int chromEnd)
    {
        // look for region
        int tid = tbx_name2id(m_tbx, chrom);
        m_iter = tbx_itr_queryi(m_tbx, tid, chromStart, chromEnd);
        if (!m_iter) {
            mexPrintf("%s:%d-%d is not found\n", chrom, chromStart, chromEnd);
            return;
        }
        
        
        // parse file
        int chromSpan = chromEnd - chromStart;
        kstring_t str = {0,0,0};
        
        while(tbx_itr_next(m_fh, m_tbx, m_iter, &str) >= 0) {
            
            char regChrom[32];
            int regStart, regEnd, regDepth;
            sscanf(str.s, "%s %d %d %d", regChrom, &regStart, &regEnd, &regDepth);
            
            
            for(int i = regStart; i < regEnd; i++) {
                
                int j = i - chromStart;
                if ((0 <= j) && (j < chromSpan))
                    depth[j] = regDepth;
            }
            
            
        }
        
        if (str.s)
            free(str.s);
        
    }
    
    
private:
    bool m_isOpen;
    htsFile *m_fh;
    tbx_t *m_tbx;
    hts_itr_t *m_iter;
    int *m_buffer;
    
};

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // Get the command string
    char method[64];
	if (nrhs < 1 || mxGetString(prhs[0], method, sizeof(method)))
		mexErrMsgTxt("First input should be a command string less than 64 characters long.");
        
    // New
    if (!strcmp("new", method)) {
        // Check parameters
        if (nlhs != 1)
            mexErrMsgTxt("New: One output expected.");
        // Return a handle to a new C++ instance
        plhs[0] = convertPtr2Mat<TabixInterface>(new TabixInterface);
        return;
    }
    
    // Check there is a second input, which should be the class instance handle
    if (nrhs < 2)
		mexErrMsgTxt("Second input should be a class instance handle.");
    
    // Delete
    if (!strcmp("delete", method)) {
        // Destroy the C++ object
        destroyObject<TabixInterface>(prhs[1]);
        // Warn if other commands were ignored
        if (nlhs != 0 || nrhs != 2)
            mexWarnMsgTxt("Delete: Unexpected arguments ignored.");
        return;
    }
    
    // Get the class instance pointer from the second input
    TabixInterface* handle = convertMat2Ptr<TabixInterface>(prhs[1]);
    
    
    // Call the various class methods
    
    
    // open(const char *)    
    if (!strcmp("open", method)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("Open: Unexpected arguments.");
        
        // Get file name
        char fileName[256];
        
        if (nrhs < 3 || mxGetString(prhs[2], fileName, sizeof(fileName)))
            mexErrMsgTxt("Second input should be a file name string less than 256 characters long.");
        
        // Call the method
        plhs[0] = mxCreateNumericMatrix(1, 1, mxLOGICAL_CLASS, mxREAL);
        bool *result = (bool*)mxGetData(plhs[0]);
        *result = handle->open(fileName);
        
        return;
    }
    
    
    // isOpen()    
    if (!strcmp("isOpen", method)) {
        
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("isOpen: Unexpected arguments.");
        
        // Call the method
        plhs[0] = mxCreateNumericMatrix(1, 1, mxLOGICAL_CLASS, mxREAL);
        bool *result = (bool*)mxGetData(plhs[0]);
        *result = handle->isOpen();
        
        return;
    }
    
    
    // query(const char *chrom, int chromStart, int chromEnd)
    if (!strcmp("query", method)) {
    
        // Check parameters
        if (nlhs < 0 || nrhs != 5)
            mexErrMsgIdAndTxt("TabixInterface:query:checkParameters",
                          "expected 3 input arguments and 1 output argument");
        
        // Get parameters
        char chrom[32];
        if (mxGetString(prhs[2], chrom, sizeof(chrom)))
            mexErrMsgTxt("First argument should be a string less than 32 characters long.");
        
        
        int chromStart;
        if (!mxIsDouble(prhs[3]) || mxIsComplex(prhs[3]) || mxGetN(prhs[3]) * mxGetM(prhs[3]) != 1) {
            mexErrMsgIdAndTxt("TabixInterface:query:fieldNotScalar",
                          "Second input argument must be a real scalar value.");
        }
        chromStart = mxGetScalar(prhs[3]);
        
        int chromEnd;
        if (!mxIsDouble(prhs[4]) || mxIsComplex(prhs[4]) || mxGetN(prhs[4]) * mxGetM(prhs[4]) != 1) {
            mexErrMsgIdAndTxt("TabixInterface:query:fieldNotScalar",
                          "Third input argument must be a real scalar value.");
        }
        chromEnd = mxGetScalar(prhs[4]);
        
        // allocate depth vector
        int chromSpan = chromEnd - chromStart;
        plhs[0] = mxCreateNumericMatrix(chromSpan, 1, mxINT32_CLASS, mxREAL);
        int *depth = (int *)mxGetData(plhs[0]);
        handle->query(depth, chrom, chromStart, chromEnd);
        
        return;
    }
    
    // Got here, so command not recognized
    mexErrMsgTxt("Command not recognized.");
}
