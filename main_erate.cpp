#include <iostream>
#include <numeric>
#include <algorithm>

#include "argumentparser.h"
#include "bamio.h"
#include "bedio.h"
#include "version.h"


int main_erate(const int argc, const char *argv[])
{
    std::string fileBed;
    std::vector<std::string> filesBam;
    std::size_t spanBases;
    std::size_t spanCodons;
    std::size_t offsetBases;
    std::size_t offsetCodons;

    auto p = ArgumentParser("erate", std::string(VERSION), "calculates coverage of required segment to be used for elongation rate prediction");
    p.addArgumentRequired("annotation").setKeyShort("-a").setKeyLong("--bed").setHelp("BED file containing transcript annotation");
    p.addArgumentPositional("alignment").setKeyShort("-b").setKeyLong("--bam").setHelp("BAM file for alignment").setCount(-1);
    p.addArgumentOptional("span").setKeyShort("-l").setKeyLong("--length").setHelp("size of metagene histogram in codons").setDefaultValue<std::size_t>(400);
    p.addArgumentOptional("offset").setKeyShort("-o").setKeyLong("--offset").setHelp("initiation and termination offset in codons").setDefaultValue<std::size_t>(20);
    p.addArgumentFlag("help").setKeyShort("-h").setKeyLong("--help").setHelp("prints help message");
    p.addArgumentFlag("version").setKeyShort("-v").setKeyLong("--version").setHelp("prints major.minor.build version");

    try {
        p.parse(argc, argv);
        fileBed = p.get<std::string>("annotation");
        filesBam = p.get<std::vector<std::string>>("alignment");
        spanCodons = p.get<std::size_t>("span");
        offsetCodons = p.get<std::size_t>("offset");
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }


    auto hBed = BedIO(fileBed);
    if (!hBed.isOpen()) {
        std::cerr << "ribotools::" + hBed.error() << std::endl;
        return EXIT_FAILURE;
    }


    auto hBam = BamIO(filesBam);
    if (!hBam.isOpen()) {
        std::cerr << "ribotools::" + hBam.what() << std::endl;
        return EXIT_FAILURE;
    }

    // convert offset and span to nucleotides;
    offsetBases = offsetCodons * 3;
    spanBases = spanCodons * 3;

    // loop over each bed record
    std::vector<float> buffer(spanCodons * hBam.aux.size(), 0.0f);

    int countRecords = 0;

    while (hBed.next()) {

        // filter short records
        int minOrfSpan = static_cast<int>(spanBases + 2*offsetBases);
        if (hBed.bed().orfSpan()  < minOrfSpan)
            continue;

        std::size_t bamId = 0;
        float background = 1.0f;
        bool flagIsUsable = true;
        std::vector<float> data(spanCodons * hBam.aux.size(), 0.0f);

        for(auto &aux : hBam.aux) {

            // pileup coverage per base
            std::vector<int> covBases = aux->depth(hBed.bed().name(1),
                                                   hBed.bed().orfStart() + static_cast<int>(offsetBases),
                                                   hBed.bed().orfEnd() - static_cast<int>(offsetBases));

            // reduce to codons
            std::vector<float> covCodons(covBases.size()/3, 0);
            const float codon_step = 3.0f;

            for (std::size_t i = 0; i < covCodons.size(); ++i) {
                covCodons.at(i) = (covBases.at(3*i) + covBases.at(3*i+1) + covBases.at(3*i+2)) / codon_step;
            }

            // normalize to mean
            float baseline = std::accumulate(covCodons.begin(), covCodons.end(), 0) / (covCodons.end() - covCodons.begin());
            if (baseline < 1) {
                flagIsUsable = false;
                break;
            }
            //std::transform(covCodons.begin(), covCodons.end(), covCodons.begin(), [baseline](float base)->float{return base / baseline;});

            // calculate background
            if (bamId == 0) {
                float backgroundSize = static_cast<float>(covCodons.size() - spanCodons);
                if (backgroundSize > 0)
                    background = std::accumulate(covCodons.begin() + static_cast<int>(spanCodons), covCodons.end(), 0) / backgroundSize;
            }

            if (background < 0.001f) {
                flagIsUsable = false;
                break;
            }

            std::transform(covCodons.begin(), covCodons.end(), covCodons.begin(), [background](float base)->float{return base / background;});


            // add to buffer
            int shiftBegin = static_cast<int>(bamId * spanCodons);
            int shiftEnd = shiftBegin + static_cast<int>(spanCodons);
            std::transform(data.begin() + shiftBegin,
                           data.begin() + shiftEnd,
                           covCodons.begin(),
                           data.begin() + shiftBegin,
                           [](float vData, float vCov)->float{return vData + vCov;});


            bamId++;
        }

        if (flagIsUsable) {
            // transfer to buffer
            std::transform(buffer.begin(),
                           buffer.end(),
                           data.begin(),
                           buffer.begin(),
                           [](float vData, float vCov)->float{return vData + vCov;});

            countRecords++;
        }


    }

    // print matrix
    for (std::size_t i = 0; i < hBam.aux.size(); i++) {
        std::cout << hBam.aux[i]->name() << "\t" << countRecords << "\t";
        for (std::size_t j = 0; j < spanCodons; j++) {
            std::cout << buffer.at(i * spanCodons + j) / countRecords << ",";
        }
        std::cout << std::endl;
    }



    return 0;
}
