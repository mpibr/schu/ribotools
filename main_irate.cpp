#include <iostream>
#include <memory>
#include <fstream>
#include <unordered_map>

#include "argumentparser.h"
#include "bedio.h"
#include "bamio.h"
#include "version.h"

/*
void loadAndCountBamHandles(std::vector<std::shared_ptr<BamHandle>> &handlesBam, const std::vector<std::string> &filesBam)
{
    for (const auto &fileName : filesBam) {
        std::shared_ptr<BamHandle> handle = std::make_shared<BamHandle>(fileName, 0, 0);
        handle->countUniqueReads();
        std::cerr << handle->name() << "\t" << handle->reads() << std::endl;
        handlesBam.emplace_back(handle);
    }
}
*/

int main_irate(const int argc, const char *argv[])
{
    std::string fileBed;
    std::vector<std::string> filesBam_rfp;
    std::vector<std::string> filesBam_rna;

    auto p = ArgumentParser("irate", std::string(VERSION), "calculate initiation rate from Ribo Footprint and RNA coverage");
    p.addArgumentRequired("annotation").setKeyShort("-a").setKeyLong("--bed").setHelp("BED file containing transcript annotation");
    p.addArgumentRequired("RFP").setKeyShort("-f").setKeyLong("--rfp").setHelp("BAM files of ribosome footrpint coverage").setCount(-1);
    p.addArgumentRequired("RNA").setKeyShort("-n").setKeyLong("--rna").setHelp("BAM files of RNA coverage").setCount(-1);
    p.addArgumentFlag("help").setKeyShort("-h").setKeyLong("--help").setHelp("prints help message");
    p.addArgumentFlag("version").setKeyShort("-v").setKeyLong("--version").setHelp("prints major.minor.build version");

    try {
        p.parse(argc, argv);
        fileBed = p.get<std::string>("annotation");
        filesBam_rfp = p.get<std::vector<std::string>>("RFP");
        filesBam_rna = p.get<std::vector<std::string>>("RNA");
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    // open BAM handles
    auto hBam_rfp = BamIO(filesBam_rfp);
    if (!hBam_rfp.isOpen()) {
        std::cerr << "ribotools::irate::error, failed to open ribosome footrpints BAM file " << std::endl;
        std::cerr << "\t" << hBam_rfp.what() << std::endl;
        return EXIT_FAILURE;
    }

    auto hBam_rna = BamIO(filesBam_rna);
    if (!hBam_rna.isOpen()) {
        std::cerr << "ribotools::irate::error, failed to open RNA BAM file " << std::endl;
        std::cerr << "\t" << hBam_rna.what() << std::endl;
        return EXIT_FAILURE;
    }

    // check if handles size is matching
    if (hBam_rfp.aux.size() != hBam_rna.aux.size()) {
        std::cerr << "ribotools::irate::error, provide equal number of BAM files for Ribosome Footprint(--rfp) and RNASeq(--rna)" << std::endl;
        return EXIT_FAILURE;
    }

    // open BED file
    auto hBed = BedIO(fileBed);
    if (!hBed.isOpen()) {
        std::cerr << "ribotools::irate::error, failed to open BED file" << std::endl;
        std::cerr << "\t" << hBed.error() << std::endl;
        return EXIT_FAILURE;
    }

    // allocate raw counts
    std::vector<int> rawCounts_rfp(hBam_rfp.aux.size(), 0);
    std::vector<int> rawCounts_rna(hBam_rna.aux.size(), 0);
    std::size_t fileIndex = 0;
    std::vector<std::shared_ptr<BamAuxiliary>>::iterator iteratorRFP;
    std::vector<std::shared_ptr<BamAuxiliary>>::iterator iteratorRNA;
    for (iteratorRFP = hBam_rfp.aux.begin(), iteratorRNA = hBam_rna.aux.begin();
         (iteratorRFP != hBam_rfp.aux.end()) && (iteratorRNA != hBam_rna.aux.end());
         ++iteratorRFP, ++iteratorRNA) {
        rawCounts_rfp[fileIndex] = (*iteratorRFP)->count();
        rawCounts_rna[fileIndex] = (*iteratorRNA)->count();
        fileIndex++;
    }



    // loop over bed records
    while (hBed.next()) {

        fileIndex = 0;
        std::vector<int> orfCounts_rna(hBam_rna.aux.size(), 0);
        std::vector<int> orfCounts_rfp(hBam_rfp.aux.size(), 0);
        std::vector<int> orfCounts_ini(hBam_rfp.aux.size(), 0);


        for (iteratorRFP = hBam_rfp.aux.begin(), iteratorRNA = hBam_rna.aux.begin();
             (iteratorRFP != hBam_rfp.aux.end()) && (iteratorRNA != hBam_rna.aux.end());
             ++iteratorRFP, ++iteratorRNA) {

            // count RNA
            if((*iteratorRNA)->query(hBed.bed().name(1), hBed.bed().orfStart(), hBed.bed().orfEnd()))
                orfCounts_rna[fileIndex] = (*iteratorRNA)->count();

            // count ORF
            if((*iteratorRFP)->query(hBed.bed().name(1), hBed.bed().orfStart(), hBed.bed().orfEnd()))
                orfCounts_rfp[fileIndex] = (*iteratorRFP)->count();

            // count initiation segment
            if ((*iteratorRFP)->query(hBed.bed().name(1), hBed.bed().orfStart(), hBed.bed().orfStart() + 33))
                orfCounts_ini[fileIndex] = (*iteratorRFP)->count();

            // next file
            fileIndex++;
        }


        // write our result
        std::cout << hBed.bed().name(0) << "\t" << hBed.bed().span() << "\t" << hBed.bed().orfSpan();
        for (std::size_t f = 0; f < fileIndex; ++f) {
            std::cout << "\t" << rawCounts_rna[f];
            std::cout << "\t" << orfCounts_rna[f];
            std::cout << "\t" << rawCounts_rfp[f];
            std::cout << "\t" << orfCounts_rfp[f];
            std::cout << "\t" << orfCounts_ini[f];
        }

        std::cout << std::endl;


        /*
        for (iteratorRFP = handlesBam_rfp.begin(), iteratorRNA = handlesBam_rna.begin();
             (iteratorRFP != handlesBam_rfp.end()) && (iteratorRNA != handlesBam_rna.end());
             ++iteratorRFP, ++iteratorRNA) {

            std::vector<int> rfpc(static_cast<size_t>(bed.cdsSpan), 0);
            int reads_rna = (*iteratorRNA)->readsPerRegion(bed.transcript, 0, bed.span);

            (*iteratorRFP)->calculateSiteCoverage(rfpc, bed.transcript, bed.cdsStart, bed.cdsEnd, bed.cdsStart, true);
            int reads_rfp = 0;
            int reads_ini = 0;
            for (size_t k = 3; k < rfpc.size(); ++k) {

                reads_rfp += rfpc.at(k);

                if (k < 33)
                    reads_ini += rfpc.at(k);

            }

            std::cout << "\t" << (*iteratorRFP)->reads() << "\t" << reads_rfp << "\t" << reads_ini << "\t" <<
                         (*iteratorRNA)->reads() << "\t" << reads_rna;
        }
        */


    }


    return 0;
}
